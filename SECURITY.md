# Security Policy

The following is a summary of Librebook's terms of service for any hostees and for users to see how their data is expected to be treated.

## Librebook hosting TOS

By hosting Librebook you agree to our Terms Of Service which are as follows:
- You will NOT collect any uneeded user data (real name, address, IP, phone number)
- You will NOT sell any user data
- You will NOT claim Librebook to be your own product or legal prosecution may follow.


If these terms of service are violated the following steps will be taken in this order.
1. You will recieve an email from the creator of Librebook requesting for you to ammend these issues.
2. If the issue continues you will recieve another email from the creator of Librebook requesting that your instance is taken down. Your instance will no longer be approved if it was.
3. If the actions are serious and continue legal prosectution may follow.


While legal actions may seem excessive Librebook handles peoples data and unlawful, unnecessary over collection (real name, address, IP, phone number) and distribution is a wrong and goes against our values.
